void setup()//initialization
{
  size(1920,1080,P3D);
  camera(0,0, -(height/2) / tan(PI * 30 /180), //camera position
            0, 0,0 , //eye position
            0, -1 ,0); // up vector
            
  yValues = new float[w/xSpacing];
}


void draw()//called every frame
{
  background(130);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  drawSineWave();
}  

void drawCartesianPlane()
{
  line(300,0,-300,0);
  line(0,300, 0, -300);
  
  for (int i= -300; i <= 300; i+=10)
  {
    line (i, -5, i, 5);
    line (-5, i , 5 , i);
  }
}

void drawLinearFunction()
{
  /*
    f(x) = x + 2
    Let x be 4, then y = 6 (4,6)
    Let x be -5 , then y = -3 (-5, -3)
  */
  
  for ( int x = -200; x <= 200; x++)
  {
    circle(x, x+2, 1);
  }
}
void drawQuadraticFunction()
{
  /*
    f(x) = 10x^2 + 2x - 5 
  */
  for (float x = -300; x <=300; x+=0.1)
  {
    circle(x * 10, (float)Math.pow(x,2) + (2 * x) - 5, 1); // typeCast
  }
}
float radius = 50;

void drawCircle()
{
  for ( int x = 0; x <360; x++)  
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
  }
}

  int w = width+16;
  int xSpacing = 20;
  
  float theta = 0.0;
  float amplitude =  75.0;
  float period = 100.0;
  float dx = (TWO_PI / period) * xSpacing;
  float[] yValues;
  float x = theta;
  
  
void drawSineWave()
{
  theta += 0.02;
 
 
  for (int i = 0; i <yValues.length; i++)
  {
    yValues[i] = sin(x) * amplitude;
    x+=dx;
  }
   
  for (int x = 0; x < yValues.length; x++)
  {
    ellipse (x * xSpacing, height/2 + yValues[x], 16, 16);
  }
}
