public class Mover
{
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass = 1;
  public float scale = 50;
  public float r = 255, g = 255, b = 255, a = 255;
   Mover()
   {
      position = new PVector(); 
   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
      this.scale = scale;
   }
   
   Mover(PVector position)
   {
      this.position = position; 
   }
   
   Mover(PVector position, float mass)
   {
      this.position = position; 
      this.mass = mass;
   }
   
   
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, this.mass * 10); 
   }
   
   public void randomWalk()
   {
      float decision = random(0, 4);
      
      if (decision == 0)
      {
         position.x ++; 
      }
      else if (decision == 1)
      {
         position.x --; 
      }
      else if (decision == 2)
      {
         position.y ++; 
      }
      else if (decision == 3)
      {
         position.y --; 
      }
   }
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
  public void update()
  {
    
    this.velocity.add(this.acceleration);
    
    this.velocity.limit(30);
    // add the velocit
    this.position.add(this.velocity);
    
    this.acceleration.mult(0);
  }
  public void applyForce(PVector force)
  {
    PVector f = PVector.div(force,this.mass);
    this.acceleration.add(f);
    
  }
  public void applyGravity()
  {
    PVector gravity = new PVector (0, -0.15f * this.mass,0);
    this.applyForce(gravity);
  }
  public void applyFriction()
  {
    float c = 0.1;
    float normal = 1;
    float frictionMagnitude = c * normal;
    
    PVector friction = this.velocity.copy();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    this.applyForce(friction);
  }
}
