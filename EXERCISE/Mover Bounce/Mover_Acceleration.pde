Mover mover;
Mover[] movers = new Mover[10];

void setup()
{
  size(1080,720,P3D);
  camera(0,0,Window.eyeZ,0,0,0,0,-1,0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1,0);
  
  for (int i = 0; i < 10 ; i++)
  {
    movers[i] = new Mover(Window.left + 50, 200);
    movers[i].mass = i + 1;
    movers[i].setColor(random(1,255),random(1,225),random(1,255), random(150,255)); 
  }
}

PVector wind = new PVector(0.1f,0);
PVector gravity = new PVector(0,-0.5f);

void draw()
{
  background(255);
  noStroke();
 /* mover.update();
  mover.render();
  mover.setColor(100,0,200,255);
  
  mover.applyForce(wind);
  mover.applyForce(gravity);*/
  for (int i = 0 ; i < 10 ; i++)
  {
    
    movers[i].applyForce(wind);
    movers[i].applyFriction();
    movers[i].applyGravity();
  
    movers[i].render();
    movers[i].update();
   
 
    if (movers[i].position.y < Window.bottom)
    {
      movers[i].velocity.y *= -1;
      movers[i].position.y = Window.bottom;
    }
    if (movers[i].position.x > Window.right)
    {
      movers[i].velocity.x *= -1; 
      movers[i].position.x = Window.right;
    }
    if (movers[i].position.x < Window.left)
    {
      movers[i].velocity.x *= 1; 
      movers[i].position.x = Window.left;
    }
  }
}
