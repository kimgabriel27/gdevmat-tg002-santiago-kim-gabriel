  Mover mover;
Mover[] movers = new Mover[10];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.mass = 10;
  //mover.acceleration = new PVector(0.1, 0);
  
  for (int i = 0; i < 5; i++)
  {
    movers[i] = new Mover(Window.left + 50, 200 - (i*100));
    movers[i].acceleration = new PVector(0.1,0);
    movers[i].mass = (i*2) + 3;
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1f, 0);


void draw()
{
  background(255);
  
  noStroke();
 for (int i = 0 ; i < 5 ; i++)
  {
    
    movers[i].applyForce(wind);
   
    movers[i].render();
    movers[i].update();
   
 


    if (movers[i].position.x < Window.left)
    {
      movers[i].velocity.x *= 1; 
      movers[i].position.x = Window.left;
    }
    if(movers[i].position.x > Window.right/2 )
    {    
      movers[i].applyFriction();
    }
  }
  

}
