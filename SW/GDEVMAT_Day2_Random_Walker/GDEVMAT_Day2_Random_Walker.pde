void setup()//initialization
{
  size(1080,720,P3D);
  camera(0,0, -(height/2) / tan(PI * 30 /180), //camera position
            0, 0,0 , //eye position
            0, -1 ,0); // up vector
  
}

Walker walker = new Walker();

void draw()//called every frame
{  
  walker.render();
  float randomValue = floor(random(4));
  println(randomValue);
  
  walker.randomWalk();
}  
