public class Vector2
{ 
  public float x;
  public float y; 
  
  Vector2()
  {
    this.x = 0;
    this.y = 0;
  }
  Vector2(float x, float y)
  {
    this.x = x;
    this.y = y;
  }
  
  public void addVec(Vector2 v)
  {
    this.x += v.x;
    this.y += v.y;
  }
  public void subVec(Vector2 v)
  {
    this.x -= v.x;
    this.y -= v.y;
  }
  public void multVec(float scalar)
  {
    this.x *= scalar;
    this.y *= scalar;
    
  }
  public void divVec(float scalar)
  {
    this.x /= scalar;
    this.y /= scalar;
  }
  public float mag()
  {
    return sqrt((x*x) + (y*y));
  }
  
  public Vector2 normalize()
  {
    float mag = this.mag();
    
    if (mag > 0)
      this.divVec(this.mag());
      
    //if (x != 0) this.x /= magnitude;
    //if (y != 0) this.y /= magnitude;
    return this;
  }
}
