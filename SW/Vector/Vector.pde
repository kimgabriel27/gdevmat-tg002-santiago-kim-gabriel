  
void setup()//initialization
{
  size(1080,720,P3D);
  camera(0,0, -(height/2) / tan(PI * 30 /180), //camera position
            0, 0,0 , //eye position
            0, -1 ,0); // up vector
  //background(1);
}

Vector2 velocity = new Vector2(5,8);
Walker pos = new Walker();

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  
  return new Vector2(x,y);
}
void draw()
{
  background(1);
  
  Vector2 mouse = mousePos();
  Vector2 handle = mousePos();
  Vector2 handle2 = mousePos();
  Vector2 saber = mousePos();
  handle2.normalize();
  handle2.multVec(45);
  
  saber.normalize();
  saber.multVec(500);
  
  handle.normalize();
  handle.multVec(50);
  
  mouse.normalize();
  mouse.multVec(500);
  
  strokeWeight(10);
  stroke(200,0,0, 150);
  line (0,0,mouse.x, mouse.y);
  
  strokeWeight(13);
  stroke(87);
  line (0,-1,handle.x,handle.y);
  
  strokeWeight(11);
  stroke(20);
  line (0,0,handle2.x,handle2.y);
  
  strokeWeight(5);
  stroke(200, random(200,250));
  line (handle.x, handle.y,saber.x,saber.y);
 
  
  println("Magnitude: " + mouse.mag());
  

}
