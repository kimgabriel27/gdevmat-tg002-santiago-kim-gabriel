

Mover mover = new Mover(0, 200);
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
Mover[] movers = new Mover[10];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  mover.setColor(155, 105, 50, 255);
  mover.mass = 5;
   for (int i = 0; i < 10 ; i++)
  {
    movers[i] = new Mover((Window.left + 50) + (i * 100), 200);
    movers[i].mass = 10 - i;
    movers[i].setColor(random(1,255),random(1,225),random(1,255), random(150,255)); 
  }
}
PVector wind = new PVector(0.1f,0);
PVector gravity = new PVector(0,-0.5f);


void draw()
{
  background(255);
  
  ocean.render();
  noStroke();

  for (int i = 0 ; i < 10 ; i++)
  {
    
    movers[i].applyForce(wind);
    movers[i].applyFriction();
    movers[i].applyGravity();
  
    movers[i].render();
    movers[i].update();
   
 
    if (movers[i].position.y < Window.bottom)
    {
      movers[i].velocity.y *= -1;
      movers[i].position.y = Window.bottom;
    }
    if (movers[i].position.x > Window.right)
    {
      movers[i].velocity.x *= -1; 
      movers[i].position.x = Window.right;
    }
    if (movers[i].position.x < Window.left)
    {
      movers[i].velocity.x *= 1; 
      movers[i].position.x = Window.left;
    }
    if (ocean.isCollidingWith(movers[i]))
    {
      movers[i].applyForce(ocean.calculateDragForce(movers[i])); 
    }
  }
  

}
