class Walker
{
  float tx = 0;
  float ty = 10;
  float ts = 5;
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  
  void perlinWalk()
   {
 
    
  
    r_t += 0.1f;
    g_t += 0.2f;
    b_t += 0.3f; 
    
    float rNoise = noise(r_t);
    float gNoise = noise(g_t);
    float bNoise = noise(b_t);
    float mapR = map(rNoise,0,1,0,255);
    float mapG = map(gNoise,0,1,0,255);
    float mapB = map(bNoise,0,1,0,255);
      
    tx += 0.009  ; // rate of change
    ty += 0.009;
    ts += 0.01;
    
    float xPosition = noise(tx);
    float yPosition = noise(ty);
    float size = noise(ts);
    xPosition = map(xPosition, 0, 1, 0, width);  
    yPosition = map(yPosition, 0, 1, 0, height);
    size = map(size,0,1,0,100);
    
    fill (mapR,mapG,mapB,100);
    circle (xPosition, yPosition,size);
    
  //  int decision = floor(random(4));
   
     //println(decision);
      
    /* fill (mapR,mapG,mapB);
       if (decision == 0)
       {
          yPosition+=yNewPos;
       }
       else if (decision == 1)
       {
         yPosition-=yNewPos;
       }
       else if (decision == 2)
       {
         xPosition+=xNewPos;
       }
       else if (decision == 3)
       {
        xPosition-=xNewPos; 
       }
    */
   }
}
