void setup ()
{
  size(1080,720,P3D);
  background(255);
  camera(0,0,-(height/2.0f)/ tan(PI * 30.0f/180.0f), 0,0,0, 0,-1,0);
}

void draw ()
{
  float mean = random(50);
  float std = 200;
  float gauss = randomGaussian();
  
  float x = (std * gauss) + mean;
  float y = random(-500,500);
  
  float diameterMean = random(50);
  float diameterStd = random(100);
  float gauss2 = randomGaussian();
  
  float diameter = gauss2 * diameterStd + diameterMean;
 
  noStroke();
  fill(random(255),random(255),random(250),random(10,50));
  circle(x,y,diameter);
  
  println(frameCount);
  
  if(frameCount >= 1000)
   {
     setup();
     frameCount = -1;
   }
  
}
